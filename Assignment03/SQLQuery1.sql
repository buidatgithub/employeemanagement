create database employeeAs03

INSERT into employee (first_name,last_name,gender,dob,phoneNumber,address,department_name,remark)
VALUES ('firstname1','lastname1',0,'2001-02-21','0123456781','address1','depaetment1','remark1'),
		('firstname2','lastname2',0,'2002-02-21','0123456782','address2','depaetment2','remark2'),
		('firstname3','lastname3',1,'2003-02-21','0123456783','address3','depaetment3','remark3'),
		('firstname4','lastname4',1,'2004-02-21','0123456784','address4','depaetment4','remark4'),
		('firstname5','lastname5',1,'2005-02-21','0123456785','address5','depaetment5','remark5');

INSERT into account (account,email,password,status,employee_id)
VALUES ('account1','email1@gmail.com','password1',0,2),
		('account2','email2@gmail.com','password2',1,3),
		('account3','email3@gmail.com','password3',1,4);