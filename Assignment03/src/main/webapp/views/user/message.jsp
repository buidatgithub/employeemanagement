<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Document</title>
  <link
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
          rel="stylesheet"
  />
  <link
          href="https://getbootstrap.com/docs/5.3/assets/css/docs.css"
          rel="stylesheet"
  />
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
  <script
          src="https://kit.fontawesome.com/1b7aa09c00.js"
          crossorigin="anonymous"
  ></script>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>
<c:if test="${message != null}">
  <div class="mt-5 w-25 position-absolute z-1 top-0 start-50 translate-middle" id="alert">
    <div class="alert alert-primary d-flex justify-content-between align-items-center" role="alert">
      <span id="alert-content" class="text-danger"> ${message} </span>
      <div id="cancel-alert">
        <i class="fa-solid fa-xmark" ></i>
      </div>
    </div>
  </div>

</c:if>
<script>
      function clearAlert() {
        console.log("click")
        $("#alert").hide();
      }
      $("#cancel-alert").on("click",clearAlert)
</script>

</body>
</html>


