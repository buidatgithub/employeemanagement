<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/3186023da8.js" crossorigin="anonymous"></script>
    <style>

    </style>
</head>
<body>
<div class="header px-5 d-flex flex-row justify-content-between align-items-center text-bg-light p-2 border">
    <span class="logo text-uppercase fw-bold"><i class="fa-solid fa-users"></i> Employee</span>
    <div class="text-bg-light">
        <span class="pe-4">Helllo </span>
        <a class="text-decoration-none" href="<%= request.getContextPath()%>/admin/logout" type="button">
            <i class="fa-solid fa-right-from-bracket"></i> logout
        </a>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
        crossorigin="anonymous"></script>
<script src="
        https://cdn.jsdelivr.net/npm/jquery@3.7.0/dist/jquery.min.js
        "></script>

<script>

</script>

</body>
</html>

