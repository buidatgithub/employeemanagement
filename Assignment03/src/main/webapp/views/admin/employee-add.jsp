<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
    />
    <link
            href="https://getbootstrap.com/docs/5.3/assets/css/docs.css"
            rel="stylesheet"
    />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <script
            src="https://kit.fontawesome.com/1b7aa09c00.js"
            crossorigin="anonymous"
    ></script>
</head>
<body>
<%@ include file="../user/message.jsp"%>
<div class="container-fluid p-0">
    <%@ include file="header.jsp"%>
    <div class="row p-0 m-0">
        <%@ include file="nav.jsp"%>
        <div id="view-option" class="col-9">
            <div class="p-4">
                <div class="border-bottom">
                    <h1>Add Employee</h1>
                </div>
                <form:form action="${pageContext.request.contextPath}/admin/employee-add" method="post" modelAttribute="profileDto">
                    <div>
                        <label for="firstName">First name: <span class="text-danger">(*)</span></label>
                        <form:input path="firstName" cssClass="form-control"/>
                        <form:errors path="firstName" cssClass="text-danger"/>
                    </div>
                    <div>
                        <label for="lastName">Last name: <span class="text-danger">(*)</span></label>
                        <form:input path="lastName" cssClass="form-control"/>
                        <form:errors path="lastName" cssClass="text-danger"/>
                    </div>
                    <div>
                        <label for="phoneNumber">Phone number: <span class="text-danger">(*)</span></label>
                        <form:input path="phoneNumber" cssClass="form-control"/>
                        <form:errors path="phoneNumber" cssClass="text-danger"/>
                    </div>
                    <div>
                        <label for="dob">Date of birth: <span class="text-danger">(*)</span></label>
                        <input type="date" name="dob" class="form-select" id="dob" required/>
                        <form:errors path="dob" cssClass="text-danger"/>
                    </div>
                    <div>
                        <label class="d-block">Gender: <span class="text-danger">(*)</span></label>
                        <div class="ps-4">
                            <form:radiobutton path="gender" value="1" />
                            <label  class="pe-4">Male:</label>
                            <form:radiobutton path="gender" value="0"/>
                            <label >Female:</label>
                        </div>
                    </div>
                    <div>
                        <label for="accountName">Account: <span class="text-danger">(*)</span></label>
                        <form:input path="accountName" cssClass="form-control"/>
                        <form:errors path="accountName" cssClass="text-danger"/>
                    </div>
                    <div>
                        <label for="email">Email: <span class="text-danger">(*)</span></label>
                        <form:input path="email" cssClass="form-control"/>
                        <form:errors path="email" cssClass="text-danger"/>

                    </div>
                    <div>
                        <label for="password">Password: <span class="text-danger">(*)</span></label>
                        <form:password path="password" cssClass="form-control"/>
                        <form:errors path="password" cssClass="text-danger"/>

                    </div>
                    <div>
                        <label for="address">Address:</label>
                        <form:textarea path="address" rows="2" cssClass="form-control"/>
                        <form:errors path="address" cssClass="text-danger"/>
                    </div>
                    <div>
                        <label class="d-block">Status:</label>
                        <div class="ps-4">
                            <form:checkbox path="status" />
                            <label class="pe-4">Active:</label>
                        </div>
                    </div>
                    <div>
                        <label for="departmentName">Department: <span class="text-danger">(*)</span></label>
                        <form:input path="departmentName" cssClass="form-control"/>
                        <form:errors path="departmentName" cssClass="text-danger"/>
                    </div>
                    <div>
                        <label for="remark">Remark:</label>
                        <form:input path="remark" cssClass="form-control"/>
                        <form:errors path="remark" cssClass="text-danger"/>
                    </div>

                    <div class="py-2">
                        <button class="btn btn-primary" type="button" onclick="goBack()"><i
                                class="fa-solid fa-backward text-white"></i> Back
                        </button>
                        <button class="btn btn-warning text-white" type="reset"><i
                                class="fa-solid fa-rotate-right text-white"></i> Reset
                        </button>
                        <button class="btn btn-success " type="submit"><i class="fa-solid fa-plus text-white"></i>
                            Add
                        </button>
                    </div>
                </form:form>
            </div>

        </div>
    </div>
</div>



<script>
    function goBack() {
        window.history.back();
    }
</script>

</body>
</html>

