<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Document</title>
    <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
    />
    <link
            href="https://getbootstrap.com/docs/5.3/assets/css/docs.css"
            rel="stylesheet"
    />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <script
            src="https://kit.fontawesome.com/1b7aa09c00.js"
            crossorigin="anonymous"
    ></script>
</head>
<body>

<div class="container-fluid p-0">
    <%@ include file="header.jsp" %>
    <div class="row p-0 m-0">
        <%@ include file="nav.jsp" %>
        <div id="view-option" class="col-9 " style="min-height: 100vh">
            <div class="p-4">
                <div class="border-bottom">
                    <h1>Employee List</h1>
                </div>
                <h5 class="ms-3">${messageResult}</h5>
                <form action="<%= request.getContextPath()%>/admin/employee-list">
                    <div class="float-end d-flex my-3">
                        <div class="border me-2 rounded overflow-hidden">
                            <button class="border-0 h-100 px-2 " disabled><i
                                    class="fa-solid fa-magnifying-glass"></i>
                            </button>
                            <input class="border-0 h-100" type="text" id="searchValue" name="searchValue"
                                   value="${param.searchValue}"/>
                        </div>
                        <div class="border me-2 rounded overflow-hidden">
                            <button class="border-0 h-100 " disabled><i
                                    class="fa-solid fa-filter"></i> Filter by
                            </button>
                            <select class="border-0 h-100" id="searchField" name="searchField">
                                <option ${param.searchField == "" ? "selected" : ""} value="">-- Select --</option>
                                <option ${param.searchField == "address" ? "selected" : ""} value="address">Address
                                </option>
                                <option ${param.searchField == "phoneNumber" ? "selected" : ""} value="phoneNumber">
                                    Phone number
                                </option>
                                <option ${param.searchField == "department" ? "selected" : ""} value="department">
                                    Department
                                </option>
                                <option ${param.searchField == "name" ? "selected" : ""} value="name">Name</option>
                            </select>
                        </div>
                        <button class="btn btn-primary me-2 " type="submit">Search</button>
                    </div>
                </form>
                <div class="">
                    <table class="table table-striped text-center">
                        <thead>
                        <tr>
                            <th class="bg-primary text-white" scope="col">ID</th>
                            <th class="bg-primary text-white" scope="col">Name</th>
                            <th class="bg-primary text-white" scope="col">Date of birth</th>
                            <th class="bg-primary text-white" scope="col">Address</th>
                            <th class="bg-primary text-white" scope="col">Phone number</th>
                            <th class="bg-primary text-white" scope="col">Department</th>
                            <th class="bg-primary text-white" scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        <c:forEach var="employee" items="${employees}" varStatus="loop">
                            <tr>
                                <td>${employee.id}</td>
                                <td>${employee.firstName} ${employee.lastName}</td>
                                <td>${employee.dob}</td>
                                <td>${employee.address}</td>
                                <td>${employee.phoneNumber}</td>
                                <td>${employee.departmentName}</td>
                                <td>
                                    <a href="<%= request.getContextPath()%>/admin/employee-detail?id=${employee.id}"
                                       class="text-decoration-none me-2">
                                        <i class="fa-solid fa-eye"></i>
                                        View
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>

                <div class="row justify-content-center">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination d-flex justify-content-center">
                            <li class="page-item"><a
                                    class="page-link"
                                    href="<%= request.getContextPath()%>/admin/employee-list?nowPage=${pageNumber - 1}&&searchValue=${param.searchValue}&&searchField=${param.searchField}">
                                Previous
                            </a></li>
                            <c:forEach var="p" begin="1" end="${maxPage}" varStatus="loop">
                                <li class="page-item"><a
                                        class="page-link ${pageNumber == loop.count ? "text-danger" : ""}"
                                        href="<%= request.getContextPath()%>/admin/employee-list?nowPage=${loop.count}&&searchValue=${param.searchValue}&&searchField=${param.searchField}">
                                        ${loop.count}
                                </a></li>
                            </c:forEach>
                            <li class="page-item"><a
                                    class="page-link"
                                    href="<%= request.getContextPath()%>/admin/employee-list?nowPage=${pageNumber + 1}&&searchValue=${param.searchValue}&&searchField=${param.searchField}">
                                Next
                            </a></li>
                        </ul>
                    </nav>
                </div>
            </div>

        </div>
    </div>
</div>
<c:if test="${message != null}">
    <script>
        function myFunction() {
            alert("${message}");

        }
        myFunction();
    </script>
</c:if>
</body>
</html>

