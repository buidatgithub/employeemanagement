<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/3186023da8.js" crossorigin="anonymous"></script>
    <style>

    </style>
</head>
<body>
<div class="nav-left col-3 text-bg-light p-0 border-start border-end">
    <a id="dashboard" type="button"
       class="form-control border-0 border-bottom py-2 ps-4 rounded-0 text-bg-light text-info text-decoration-none text-left">
        <i class="fa-solid fa-gauge"></i> Dashboard</a>
    <a onclick="viewsOption()" type="button"
       class="dropdown-toggle form-control border-0 border-bottom py-2 ps-4 rounded-0 text-bg-light text-info text-decoration-none">
        <i class="fa-solid fa-bars"></i> Employee manager</a>
    <div id="option">
        <a class="form-control border-0 border-bottom py-2 ps-5 rounded-0 text-bg-light text-info text-decoration-none"
        href="<%= request.getContextPath()%>/admin/employee-list">
            <i class="fa-solid fa-list"></i> List Employee
        </a>
        <a class="form-control border-0 border-bottom py-2 ps-5 rounded-0 text-bg-light text-info text-decoration-none"
        href="<%= request.getContextPath()%>/admin/employee-add">
            <i class="fa-solid fa-plus"></i> Add Employee
        </a>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
        crossorigin="anonymous"></script>
<script src="
        https://cdn.jsdelivr.net/npm/jquery@3.7.0/dist/jquery.min.js
        "></script>
<script>
    let option = $("#option");
    $(document).ready(function () {
        option.hide();
    })
    function viewsOption() {
        option.show();
    }

</script>

</body>
</html>

