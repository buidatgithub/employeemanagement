package com.springmvc.service;

import com.springmvc.entities.Account;

import java.util.List;


public interface AccountService {
    public boolean save(Account account);
    Account checkLogin(String email, String password);
}
