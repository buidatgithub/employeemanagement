package com.springmvc.service.impl;

import com.springmvc.dao.AccountDao;
import com.springmvc.entities.Account;
import com.springmvc.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AccountServiceImpl implements AccountService {
    private AccountDao accountDao;

    @Autowired
    public AccountServiceImpl(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Transactional
    @Override
    public boolean save(Account account) {
        if(accountDao.findByAccount(account.getAccountName())){
            return false;
        }
        if(accountDao.findByEmail(account.getEmail())){
            return false;
        }
        return accountDao.save(account);
    }

    @Override
    public Account checkLogin(String email, String password) {
        return accountDao.checkLogin(email,password);
    }
}
