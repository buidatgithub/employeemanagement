package com.springmvc.service.impl;

import com.springmvc.dao.EmployeeDao;
import com.springmvc.entities.Employee;
import com.springmvc.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    final private EmployeeDao employeeDao;

    @Autowired
    public EmployeeServiceImpl(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
    }

    @Transactional
    @Override
    public boolean save(Employee employee) {
        return employeeDao.save(employee);
    }


    @Override
    public Employee findOneById(Long id) {
        return employeeDao.findOneById(id);
    }


    @Override
    public List<Employee> searchEmployee(String hql, Integer pageNumber, Integer pageSize) {
        return employeeDao.searchEmployee(hql, pageNumber, pageSize);
    }

    @Override
    public Integer countSearchEmployee(String hql) {
        return employeeDao.countSearchEmployee(hql);
    }
}
