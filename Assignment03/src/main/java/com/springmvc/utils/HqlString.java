package com.springmvc.utils;

import com.springmvc.dto.ParamSearch;
import org.springframework.stereotype.Component;

@Component
public class HqlString {
    public String hqlSearchString(ParamSearch paramSearch) {
        StringBuilder hql = new StringBuilder("From Employee e ");
        if (paramSearch.getSearchValue() != null && !paramSearch.getSearchValue().isEmpty()) {
            switch (paramSearch.getSearchField()) {
                case "address":
                    hql.append("WHERE e.address = '").append(paramSearch.getSearchValue()).append("'");
                    break;
                case "phoneNumber":
                    hql.append("WHERE e.phoneNumber = '").append(paramSearch.getSearchValue()).append("'");
                    break;
                case "department":
                    hql.append("WHERE e.departmentName = '").append(paramSearch.getSearchValue()).append("'");
                    break;
                case "name":
                    hql.append("WHERE e.firstName LIKE '%").append(paramSearch.getSearchValue()).append("%'")
                            .append("OR e.lastName LIKE '%").append(paramSearch.getSearchValue()).append("%'");
                    break;
                default:
            }
        }
        return hql.toString();
    }
}
