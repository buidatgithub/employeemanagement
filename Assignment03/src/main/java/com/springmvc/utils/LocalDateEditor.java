package com.springmvc.utils;

import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class LocalDateEditor extends PropertyEditorSupport {
    @Override
    public String getAsText() {
        LocalDate date = (LocalDate) getValue();
        if(date == null){
            return "";
        }
        return date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if(text == null || text.isEmpty()){
            setValue(null);
        }
        try {
            LocalDate localDate = LocalDate.parse(text, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            setValue(localDate);
        }catch (Exception e){
            throw new IllegalArgumentException("invalid date format");
        }
    }
}
