package com.springmvc.dao;

import com.springmvc.entities.Account;

import java.util.List;


public interface AccountDao {
    boolean save(Account account);
    boolean findByAccount(String account);
    boolean findByEmail(String email);
    Account checkLogin(String email, String password);
}
