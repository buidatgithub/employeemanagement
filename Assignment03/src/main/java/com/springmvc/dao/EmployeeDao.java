package com.springmvc.dao;

import com.springmvc.entities.Employee;


import java.util.List;


public interface EmployeeDao {
    boolean save (Employee employee);
    Employee findOneById(Long id);
    List<Employee> searchEmployee(String hql, Integer pageNumber, Integer pageSize);
    Integer countSearchEmployee(String hql);
}
