package com.springmvc.dao.impl;

import com.springmvc.dao.AccountDao;
import com.springmvc.entities.Account;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AccountDaoImpl implements AccountDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public boolean save(Account account) {
        Session session = sessionFactory.getCurrentSession();
        return session.save(account) != null;
    }

    @Override
    public boolean findByAccount(String account) {
        String hql = "FROM Account a WHERE a.accountName = :account";
        Session session = sessionFactory.openSession();
        Query<Account> query = session.createQuery(hql, Account.class);
        query.setParameter("account", account);
        return query.uniqueResult() != null;
    }

    @Override
    public boolean findByEmail(String email) {
        String hql = "FROM Account a WHERE a.email = :email";
        Session session = sessionFactory.openSession();
        Query<Account> query = session.createQuery(hql, Account.class);
        query.setParameter("email", email);
        return query.uniqueResult() != null;
    }

    @Override
    public Account checkLogin(String email, String password) {
        String hql = "FROM Account a WHERE a.email = :email AND a.password = :password";
        Session session = sessionFactory.openSession();
        return session.createQuery(hql, Account.class).setParameter("email", email).setParameter("password", password).uniqueResult();
    }


}
