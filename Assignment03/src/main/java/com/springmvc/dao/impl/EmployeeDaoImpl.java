package com.springmvc.dao.impl;

import com.springmvc.dao.EmployeeDao;
import com.springmvc.entities.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public boolean save(Employee employee) {
        Session session = sessionFactory.getCurrentSession();
        return session.save(employee) != null;
    }

    @Override
    public Employee findOneById(Long id) {
        String hql = "FROM Employee e WHERE e.id = :id";
        Session session = sessionFactory.openSession();
        Query<Employee> query = session.createQuery(hql, Employee.class);
        query.setParameter("id", id);
        return query.uniqueResult();
    }

    @Override
    public Integer countSearchEmployee(String hql) {
         Session session = sessionFactory.openSession();
        return session.createQuery(hql, Employee.class).getResultList().size();
    }

    @Override
    public List<Employee> searchEmployee(String hql, Integer pageNumber, Integer pageSize) {
        Session session = sessionFactory.openSession();
        List<Employee> allResults = session.createQuery(hql, Employee.class).getResultList();

        int startIndex = (pageNumber - 1) * pageSize;
        int endIndex = Math.min(startIndex + pageSize, allResults.size());

        if (startIndex < allResults.size()) {
            return allResults.subList(startIndex, endIndex);
        } else {
            return Collections.emptyList();
        }
    }
}
