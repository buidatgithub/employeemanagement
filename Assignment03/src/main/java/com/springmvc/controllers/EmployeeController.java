package com.springmvc.controllers;

import com.springmvc.dto.ParamSearch;
import com.springmvc.dto.ProfileDto;
import com.springmvc.entities.Account;
import com.springmvc.entities.Employee;
import com.springmvc.service.AccountService;
import com.springmvc.service.EmployeeService;
import com.springmvc.utils.HqlString;
import com.springmvc.utils.LocalDateEditor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.List;


@Controller
@RequestMapping("/admin")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private HqlString hqlString;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(LocalDate.class, new LocalDateEditor());
    }

    @GetMapping("/employee-list")
    public ModelAndView getEmployeeList(@ModelAttribute ParamSearch paramSearch,
                                         @RequestParam(name = "nowPage", required = false) String nowPage) {
        ModelAndView mv = new ModelAndView("admin/employee-list");
        String hql = hqlString.hqlSearchString(paramSearch);
        int result = employeeService.countSearchEmployee(hql);
        if (result > 0) {
            int pageSize = 3;
            int maxPage = Math.round((float) result / pageSize);
            int pageNumber = (nowPage == null || Integer.parseInt(nowPage) < 1) ? 1 : Integer.parseInt(nowPage);
            pageNumber = Math.min(pageNumber, maxPage);
            List<Employee> employees = employeeService.searchEmployee(hql, pageNumber, pageSize);
            String messageResult = result + " search results";
            mv.addObject("employees", employees);
            mv.addObject("maxPage", maxPage);
            mv.addObject("pageNumber", pageNumber);
            mv.addObject("messageResult", messageResult);
        } else {
            mv.addObject("message", "Don't found Employee");
        }
        return mv;
    }

    @GetMapping("/employee-detail")
    public ModelAndView getEmployeeDetail(@RequestParam String id) {
        if (id != null) {
            Employee employee = employeeService.findOneById(Long.valueOf(id));
            if (employee != null) {
                Account account = employee.getAccount();
                ProfileDto profileDto = new ProfileDto();
                BeanUtils.copyProperties(account, profileDto);
                BeanUtils.copyProperties(employee, profileDto);
                return new ModelAndView("admin/employee-detail", "profileDto", profileDto);
            }
        }
        return new ModelAndView("admin/employee-detail", "message", "Don't found Employee");
    }

    @GetMapping("/employee-add")
    public ModelAndView getEmployeeAdd(@ModelAttribute("profileDto") ProfileDto profileDto) {
        return new ModelAndView("admin/employee-add");
    }

    @PostMapping("/employee-add")
    public ModelAndView addEmployee(@ModelAttribute("profileDto") @Validated ProfileDto profileDto, BindingResult result) {
        ModelAndView mv = new ModelAndView("admin/employee-add");
        if (result.hasErrors()){
            return mv;
        }
        Employee employee = new Employee();
        Account account = new Account();
        BeanUtils.copyProperties(profileDto, employee);
        BeanUtils.copyProperties(profileDto, account);
        employee.setAccount(account);
        account.setEmployee(employee);
        String message = accountService.save(account) ? "Success" : "Fail";
        mv.addObject("message",message);
        mv.addObject("profileDto",new ProfileDto());
        return mv;
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest req, Model model) {
        HttpSession session = req.getSession();
        session.removeAttribute("account");
        return "user/login";
    }
}
