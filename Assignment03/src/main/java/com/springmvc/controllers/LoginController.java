package com.springmvc.controllers;


import com.springmvc.dto.ParamSearch;
import com.springmvc.dto.User;
import com.springmvc.entities.Account;
import com.springmvc.entities.Employee;
import com.springmvc.service.AccountService;
import com.springmvc.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;


@Controller
@SessionAttributes("account")
@RequestMapping("/user")
public class LoginController {
    @Autowired
    private AccountService accountService;

    @Autowired
    private EmployeeController employeeController;

    @ModelAttribute("account")
    public Account setAccount() {
        return new Account();
    }

    @GetMapping("/login")
    public ModelAndView getLogin() {
        return new ModelAndView("user/login");
    }

    @PostMapping("/login")
    public ModelAndView login(@ModelAttribute("user") User user, Model model) {
        Account account = accountService.checkLogin(user.getEmail(), user.getPassword());
        model.addAttribute("account", account);
        if (account != null) {
            return employeeController.getEmployeeList(new ParamSearch(), "1");
        }
        return new ModelAndView("user/login", "message", "Email or password is incorrect");
    }
}
