package com.springmvc.entities;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class Employee implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "employee_id")
    private Long id;

    @NotNull
    @Column(name = "first_name")
    private String firstName;

    @NotNull
    @Column(name = "last_name")
    private String lastName;

    @NotNull
    @Length(max = 20)
    @Column(name = "phoneNumber")
    private String phoneNumber;

    @NotNull
    @Column(name = "dob")
    private LocalDate dob;

    @NotNull
    @Column(name = "gender")
    private Long gender;


    @Column(name = "address")
    private String address;

    @NotNull
    @Column(name = "department_name")
    private String departmentName;

    @Length(max = 1000)
    @Column(name = "remark")
    private String remark;

    @OneToOne(mappedBy = "employee", cascade = CascadeType.ALL)
    @ToString.Exclude
    private Account account;
}
