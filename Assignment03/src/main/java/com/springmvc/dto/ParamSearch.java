package com.springmvc.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ParamSearch {
    private String searchValue;
    private String searchField;
}
