package com.springmvc.dto;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ProfileDto {
    private Long id;

    @NotEmpty(message = "{NotEmpty.firstName.valid}")
    @Length(min = 5, max = 50, message = "{Length.firstName.valid}")
    private String firstName;

    @NotEmpty(message = "{NotEmpty.lastName.valid}")
    @Length(min = 5, max = 50, message = "{Length.lastName.valid}")
    private String lastName;

    @NotEmpty(message = "{NotEmpty.phoneNumber.valid}")
    private String phoneNumber;

    @Past
    @NotNull
    private LocalDate dob;

    @NotNull
    private Long gender;

    @Length(min = 15,max = 50 , message = "{Length.email.valid}")
    @NotEmpty(message = "{NotEmpty.email.valid}")
    private String email;

    @NotEmpty(message = "{NotEmpty.password.valid}")
    private String password;

    @Length(min = 10, max = 50 , message = "{Length.account.valid}")
    @NotEmpty(message = "{NotEmpty.account.valid}")
    private String accountName;

    @Length(max = 50, message = "{Length.address.valid}")
    private String address;

    @Length(max = 50, message = "{Length.department.valid}")
    @NotEmpty(message = "{NotEmpty.department.valid}")
    private String departmentName;

    private boolean status;

    @Length(max = 2000, message = "{Length.remark.valid}")
    private String remark;
}
