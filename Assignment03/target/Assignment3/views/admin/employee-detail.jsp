<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Document</title>
    <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
    />
    <link
            href="https://getbootstrap.com/docs/5.3/assets/css/docs.css"
            rel="stylesheet"
    />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <script
            src="https://kit.fontawesome.com/1b7aa09c00.js"
            crossorigin="anonymous"
    ></script>
</head>
<body>
<div class="container-fluid p-0">
    <%@ include file="header.jsp" %>
    <div class="row p-0 m-0">
        <%@ include file="nav.jsp" %>
        <div id="view-option" class="col-9 " style="min-height: 100vh">
            <div class="row justify-content-center p-4">
                <div class="border-bottom mb-5">
                    <h1>Views Employee</h1>
                </div>
                <div class="col-9 border rounded p-4">
                    <h5>${message}</h5>
                    <c:if test="${profileDto != null}">

                        <table>
                            <tr>
                                <td>First Name:</td>
                                <td class="ps-3"> ${profileDto.firstName}</td>
                            </tr>
                            <tr>
                                <td>Last Name:</td>
                                <td class="ps-3"> ${profileDto.lastName}</td>
                            </tr>
                            <tr>
                                <td>Date of birth:</td>
                                <td class="ps-3"> ${profileDto.dob}</td>
                            </tr>
                            <tr>
                                <td>Phone number:</td>
                                <td class="ps-3"> ${profileDto.phoneNumber}</td>
                            </tr>
                            <tr>
                                <td>Gender:</td>
                                <td class="ps-3"> ${profileDto.gender == 0 ? "Female" : "Male"}</td>
                            </tr>
                            <tr>
                                <td>Account:</td>
                                <td class="ps-3"> ${profileDto.accountName}</td>
                            </tr>
                            <tr>
                                <td>Email:</td>
                                <td class="ps-3"> ${profileDto.email}</td>
                            </tr>
                            <tr>
                                <td>Department :</td>
                                <td class="ps-3"> ${profileDto.departmentName}</td>
                            </tr>
                            <tr>
                                <td>Remark:</td>
                                <td class="ps-3"> ${profileDto.remark}</td>
                            </tr>
                            <tr>
                                <td>Status:</td>
                                <td class="ps-3"> ${profileDto.status == true ? "acctive" : "no-active"}</td>
                            </tr>
                        </table>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>

