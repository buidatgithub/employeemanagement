
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Document</title>
    <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
    />
    <link
            href="https://getbootstrap.com/docs/5.3/assets/css/docs.css"
            rel="stylesheet"
    />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <script
            src="https://kit.fontawesome.com/1b7aa09c00.js"
            crossorigin="anonymous"
    ></script>
</head>
<body class="bg-secondary">
<div
        class="container w-100 d-flex flex-column position-relative justify-content-center align-items-center"
        style="height: 100vh"
>
    <div class="border bg-white col-3">
        <div class="rounded-top">
            <div class="p-2 py-3 text-center">
                <h2>Member Login</h2>
            </div>
        </div>
        <form
                class="p-3"
                id="form-login"
                action="<%= request.getContextPath()%>/user/login"
                method="post"
        >
            <input
                    class="form-control mb-3"
                    type="email"
                    id="email"
                    name="email"
                    required
                    placeholder="E-mail"
            />
            <input
                    class="form-control mb-2"
                    type="password"
                    name="password"
                    id="password"
                    required
                    placeholder="Password"
            />
            <input
                    class="my-2 form-control btn btn-success"
                    type="submit"
                    name="btn-submit"
                    id="btn-login"
                    value="Login"
            />
        </form>
        <div
                class="py-3 border-top rounded-bottom bg-light d-flex justify-content-center"
        >
            <a
                    class="text-decoration-none text-center"
                    href="#"
            >
                Forgot password ?
            </a>
        </div>
    </div>
</div>
<%@include file="message.jsp" %>
</body>
</html>




